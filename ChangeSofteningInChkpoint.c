
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "checkpoint.h"

/***********************************************/
/************ FUNCTION DEFINITIONS *************/
/***********************************************/

void usage() {
	fprintf(stdout,"\n@> USAGE <@\n\n");
	fprintf(stdout,"...$ ./ChangeSofteningInChkpoint -inp <chk_in_filename>\n");
	fprintf(stdout,"                                 -out <chk_out_filename>\n");
	fprintf(stdout,"                                 -fdl <fdl_filename>\n");
	fprintf(stdout,"                                 -ge <GAS_new_softening>\n");
	fprintf(stdout,"                                 -dme <DM_new_softening>\n");
	fprintf(stdout,"                                 -se <STAR_new_softening>\n\n");
	fprintf(stdout,"The code takes the GASOLINE checkpoint chk_in_filename and\n");
	fprintf(stdout,"it copies the latter in the new checkpoint chk_out_filename\n");
	fprintf(stdout,"with the ASCII header contained in the file fdl_filename\n");
	fprintf(stdout,"(typically the file 'checkpoint.fdl' contained in the\n");
	fprintf(stdout,"'pkdgrav' directory). While reading each particle, the code\n");
	fprintf(stdout,"modifies the softening of gas, dark matter, and star particles\n");
	fprintf(stdout,"according to what is specified through GAS_new_softening,\n");
	fprintf(stdout,"DM_new_softening, and STAR_new_softening, respectively. The\n");
	fprintf(stdout,"specified parameters have to contain the value of the new\n");
	fprintf(stdout,"softening in CODE UNITS!\n\n");
	exit(EXIT_FAILURE);
	return;
}

void ReadWriteHeader(CHK_HEADER *header, FILE *in, FILE *out) {

	/* READING */

	fread(&(header->version),sizeof(header->version),1,in);
	fread(&(header->not_corrupt_flag),sizeof(header->not_corrupt_flag),1,in);
	fread(&(header->number_of_particles),sizeof(header->number_of_particles),1,in);
	fread(&(header->number_of_gas_particles),sizeof(header->number_of_gas_particles),1,in);
	fread(&(header->number_of_dark_particles),sizeof(header->number_of_dark_particles),1,in);
	fread(&(header->number_of_star_particles),sizeof(header->number_of_star_particles),1,in);
	fread(&(header->max_order),sizeof(header->max_order),1,in);
	fread(&(header->max_order_gas),sizeof(header->max_order_gas),1,in);
	fread(&(header->max_order_dark),sizeof(header->max_order_dark),1,in);
	fread(&(header->current_timestep),sizeof(header->current_timestep),1,in);
	fread(&(header->current_time),sizeof(header->current_time),1,in);
	fread(&(header->current_ecosmo),sizeof(header->current_ecosmo),1,in);
	fread(&(header->old_time),sizeof(header->old_time),1,in);
	fread(&(header->old_potentiale),sizeof(header->old_potentiale),1,in);
	fread(&(header->opening_type),sizeof(header->opening_type),1,in);
	fread(&(header->opening_criterion),sizeof(header->opening_criterion),1,in);
	fread(&(header->number_of_outs),sizeof(header->number_of_outs),1,in);

	fread(&(header->bPeriodic),sizeof(header->bPeriodic),1,in);
	fread(&(header->bComove),sizeof(header->bComove),1,in);
	fread(&(header->bParaRead),sizeof(header->bParaRead),1,in);
	fread(&(header->bParaWrite),sizeof(header->bParaWrite),1,in);
	fread(&(header->bCannonical),sizeof(header->bCannonical),1,in);
	fread(&(header->bStandard),sizeof(header->bStandard),1,in);
	fread(&(header->bKDK),sizeof(header->bKDK),1,in);
	fread(&(header->bBinary),sizeof(header->bBinary),1,in);
	fread(&(header->bDoGravity),sizeof(header->bDoGravity),1,in);
	fread(&(header->bDoGas),sizeof(header->bDoGas),1,in);
	fread(&(header->bFandG),sizeof(header->bFandG),1,in);
	fread(&(header->bHeliocentric),sizeof(header->bHeliocentric),1,in);
	fread(&(header->bLogHalo),sizeof(header->bLogHalo),1,in);
	fread(&(header->bHernquistSpheroid),sizeof(header->bHernquistSpheroid),1,in);
	fread(&(header->bMiyamotoDisk),sizeof(header->bMiyamotoDisk),1,in);
	fread(&(header->bRotatingBar),sizeof(header->bRotatingBar),1,in);

	fread(&(header->dRotBarMass),sizeof(header->dRotBarMass),1,in);
	fread(&(header->dRotBarLength),sizeof(header->dRotBarLength),1,in);
	fread(&(header->dRotBarCorotFac),sizeof(header->dRotBarCorotFac),1,in);
	fread(&(header->dRotBarTurnOff),sizeof(header->dRotBarTurnOff),1,in);
	fread(&(header->dRotBarTurnOn),sizeof(header->dRotBarTurnOn),1,in);
	fread(&(header->dRotBarAmpFac),sizeof(header->dRotBarAmpFac),1,in);
	fread(&(header->dRotBarDuration),sizeof(header->dRotBarDuration),1,in);
	fread(&(header->dRotBarPosAng),sizeof(header->dRotBarPosAng),1,in);
	fread(&(header->dRotBarPosX),sizeof(header->dRotBarPosX),1,in);
	fread(&(header->dRotBarPosY),sizeof(header->dRotBarPosY),1,in);
	fread(&(header->dRotBarPosZ),sizeof(header->dRotBarPosZ),1,in);
	fread(&(header->dRotBarVelX),sizeof(header->dRotBarVelX),1,in);
	fread(&(header->dRotBarVelY),sizeof(header->dRotBarVelY),1,in);
	fread(&(header->dRotBarVelZ),sizeof(header->dRotBarVelZ),1,in);
	fread(&(header->dRotBarTime0),sizeof(header->dRotBarTime0),1,in);
	fread(&(header->dRotBarOmega),sizeof(header->dRotBarOmega),1,in);
	fread(&(header->dRotBarB5),sizeof(header->dRotBarB5),1,in);
	fread(&(header->dRotBarIz),sizeof(header->dRotBarIz),1,in);
	fread(&(header->dRotBarLz),sizeof(header->dRotBarLz),1,in);
	fread(&(header->dRotBarLz0),sizeof(header->dRotBarLz0),1,in);

	fread(&(header->nBucket),sizeof(header->nBucket),1,in);
	fread(&(header->iOutInterval),sizeof(header->iOutInterval),1,in);
	fread(&(header->iLogInterval),sizeof(header->iLogInterval),1,in);
	fread(&(header->iCheckInterval),sizeof(header->iCheckInterval),1,in);
	fread(&(header->iExpOrder),sizeof(header->iExpOrder),1,in);
	fread(&(header->iEwOrder),sizeof(header->iEwOrder),1,in);
	fread(&(header->nReplicas),sizeof(header->nReplicas),1,in);
	fread(&(header->iStartStep),sizeof(header->iStartStep),1,in);
	fread(&(header->nSteps),sizeof(header->nSteps),1,in);

	fread(&(header->dExtraStore),sizeof(header->dExtraStore),1,in);
	fread(&(header->dDelta),sizeof(header->dDelta),1,in);
	fread(&(header->dEta),sizeof(header->dEta),1,in);
	fread(&(header->dEtaCourant),sizeof(header->dEtaCourant),1,in);

	fread(&(header->bEpsAccStep),sizeof(header->bEpsAccStep),1,in);
	fread(&(header->bNonSymp),sizeof(header->bNonSymp),1,in);
	fread(&(header->iMaxRung),sizeof(header->iMaxRung),1,in);

	fread(&(header->dEwCut),sizeof(header->dEwCut),1,in);
	fread(&(header->dEwhCut),sizeof(header->dEwhCut),1,in);
	fread(&(header->dPeriod),sizeof(header->dPeriod),1,in);
	fread(&(header->dxPeriod),sizeof(header->dxPeriod),1,in);
	fread(&(header->dyPeriod),sizeof(header->dyPeriod),1,in);
	fread(&(header->dzPeriod),sizeof(header->dzPeriod),1,in);
	fread(&(header->dHubble0),sizeof(header->dHubble0),1,in);
	fread(&(header->dOmega0),sizeof(header->dOmega0),1,in);
	fread(&(header->dLambda),sizeof(header->dLambda),1,in);
	fread(&(header->dOmegaRad),sizeof(header->dOmegaRad),1,in);
	fread(&(header->dQuintess),sizeof(header->dQuintess),1,in);
	fread(&(header->dTheta),sizeof(header->dTheta),1,in);
	fread(&(header->dTheta2),sizeof(header->dTheta2),1,in);
	fread(&(header->dCentMass),sizeof(header->dCentMass),1,in);
	fread(&(header->dMassFracHelium),sizeof(header->dMassFracHelium),1,in);
	fread(&(header->dFracNoDomainDecomp),sizeof(header->dFracNoDomainDecomp),1,in);

	/* WRITING */

	fwrite(&(header->version),sizeof(header->version),1,out);
	fwrite(&(header->not_corrupt_flag),sizeof(header->not_corrupt_flag),1,out);
	fwrite(&(header->number_of_particles),sizeof(header->number_of_particles),1,out);
	fwrite(&(header->number_of_gas_particles),sizeof(header->number_of_gas_particles),1,out);
	fwrite(&(header->number_of_dark_particles),sizeof(header->number_of_dark_particles),1,out);
	fwrite(&(header->number_of_star_particles),sizeof(header->number_of_star_particles),1,out);
	fwrite(&(header->max_order),sizeof(header->max_order),1,out);
	fwrite(&(header->max_order_gas),sizeof(header->max_order_gas),1,out);
	fwrite(&(header->max_order_dark),sizeof(header->max_order_dark),1,out);
	fwrite(&(header->current_timestep),sizeof(header->current_timestep),1,out);
	fwrite(&(header->current_time),sizeof(header->current_time),1,out);
	fwrite(&(header->current_ecosmo),sizeof(header->current_ecosmo),1,out);
	fwrite(&(header->old_time),sizeof(header->old_time),1,out);
	fwrite(&(header->old_potentiale),sizeof(header->old_potentiale),1,out);
	fwrite(&(header->opening_type),sizeof(header->opening_type),1,out);
	fwrite(&(header->opening_criterion),sizeof(header->opening_criterion),1,out);
	fwrite(&(header->number_of_outs),sizeof(header->number_of_outs),1,out);

	fwrite(&(header->bPeriodic),sizeof(header->bPeriodic),1,out);
	fwrite(&(header->bComove),sizeof(header->bComove),1,out);
	fwrite(&(header->bParaRead),sizeof(header->bParaRead),1,out);
	fwrite(&(header->bParaWrite),sizeof(header->bParaWrite),1,out);
	fwrite(&(header->bCannonical),sizeof(header->bCannonical),1,out);
	fwrite(&(header->bStandard),sizeof(header->bStandard),1,out);
	fwrite(&(header->bKDK),sizeof(header->bKDK),1,out);
	fwrite(&(header->bBinary),sizeof(header->bBinary),1,out);
	fwrite(&(header->bDoGravity),sizeof(header->bDoGravity),1,out);
	fwrite(&(header->bDoGas),sizeof(header->bDoGas),1,out);
	fwrite(&(header->bFandG),sizeof(header->bFandG),1,out);
	fwrite(&(header->bHeliocentric),sizeof(header->bHeliocentric),1,out);
	fwrite(&(header->bLogHalo),sizeof(header->bLogHalo),1,out);
	fwrite(&(header->bHernquistSpheroid),sizeof(header->bHernquistSpheroid),1,out);
	fwrite(&(header->bMiyamotoDisk),sizeof(header->bMiyamotoDisk),1,out);
	fwrite(&(header->bRotatingBar),sizeof(header->bRotatingBar),1,out);

	fwrite(&(header->dRotBarMass),sizeof(header->dRotBarMass),1,out);
	fwrite(&(header->dRotBarLength),sizeof(header->dRotBarLength),1,out);
	fwrite(&(header->dRotBarCorotFac),sizeof(header->dRotBarCorotFac),1,out);
	fwrite(&(header->dRotBarTurnOff),sizeof(header->dRotBarTurnOff),1,out);
	fwrite(&(header->dRotBarTurnOn),sizeof(header->dRotBarTurnOn),1,out);
	fwrite(&(header->dRotBarAmpFac),sizeof(header->dRotBarAmpFac),1,out);
	fwrite(&(header->dRotBarDuration),sizeof(header->dRotBarDuration),1,out);
	fwrite(&(header->dRotBarPosAng),sizeof(header->dRotBarPosAng),1,out);
	fwrite(&(header->dRotBarPosX),sizeof(header->dRotBarPosX),1,out);
	fwrite(&(header->dRotBarPosY),sizeof(header->dRotBarPosY),1,out);
	fwrite(&(header->dRotBarPosZ),sizeof(header->dRotBarPosZ),1,out);
	fwrite(&(header->dRotBarVelX),sizeof(header->dRotBarVelX),1,out);
	fwrite(&(header->dRotBarVelY),sizeof(header->dRotBarVelY),1,out);
	fwrite(&(header->dRotBarVelZ),sizeof(header->dRotBarVelZ),1,out);
	fwrite(&(header->dRotBarTime0),sizeof(header->dRotBarTime0),1,out);
	fwrite(&(header->dRotBarOmega),sizeof(header->dRotBarOmega),1,out);
	fwrite(&(header->dRotBarB5),sizeof(header->dRotBarB5),1,out);
	fwrite(&(header->dRotBarIz),sizeof(header->dRotBarIz),1,out);
	fwrite(&(header->dRotBarLz),sizeof(header->dRotBarLz),1,out);
	fwrite(&(header->dRotBarLz0),sizeof(header->dRotBarLz0),1,out);

	fwrite(&(header->nBucket),sizeof(header->nBucket),1,out);
	fwrite(&(header->iOutInterval),sizeof(header->iOutInterval),1,out);
	fwrite(&(header->iLogInterval),sizeof(header->iLogInterval),1,out);
	fwrite(&(header->iCheckInterval),sizeof(header->iCheckInterval),1,out);
	fwrite(&(header->iExpOrder),sizeof(header->iExpOrder),1,out);
	fwrite(&(header->iEwOrder),sizeof(header->iEwOrder),1,out);
	fwrite(&(header->nReplicas),sizeof(header->nReplicas),1,out);
	fwrite(&(header->iStartStep),sizeof(header->iStartStep),1,out);
	fwrite(&(header->nSteps),sizeof(header->nSteps),1,out);

	fwrite(&(header->dExtraStore),sizeof(header->dExtraStore),1,out);
	fwrite(&(header->dDelta),sizeof(header->dDelta),1,out);
	fwrite(&(header->dEta),sizeof(header->dEta),1,out);
	fwrite(&(header->dEtaCourant),sizeof(header->dEtaCourant),1,out);

	fwrite(&(header->bEpsAccStep),sizeof(header->bEpsAccStep),1,out);
	fwrite(&(header->bNonSymp),sizeof(header->bNonSymp),1,out);
	fwrite(&(header->iMaxRung),sizeof(header->iMaxRung),1,out);

	fwrite(&(header->dEwCut),sizeof(header->dEwCut),1,out);
	fwrite(&(header->dEwhCut),sizeof(header->dEwhCut),1,out);
	fwrite(&(header->dPeriod),sizeof(header->dPeriod),1,out);
	fwrite(&(header->dxPeriod),sizeof(header->dxPeriod),1,out);
	fwrite(&(header->dyPeriod),sizeof(header->dyPeriod),1,out);
	fwrite(&(header->dzPeriod),sizeof(header->dzPeriod),1,out);
	fwrite(&(header->dHubble0),sizeof(header->dHubble0),1,out);
	fwrite(&(header->dOmega0),sizeof(header->dOmega0),1,out);
	fwrite(&(header->dLambda),sizeof(header->dLambda),1,out);
	fwrite(&(header->dOmegaRad),sizeof(header->dOmegaRad),1,out);
	fwrite(&(header->dQuintess),sizeof(header->dQuintess),1,out);
	fwrite(&(header->dTheta),sizeof(header->dTheta),1,out);
	fwrite(&(header->dTheta2),sizeof(header->dTheta2),1,out);
	fwrite(&(header->dCentMass),sizeof(header->dCentMass),1,out);
	fwrite(&(header->dMassFracHelium),sizeof(header->dMassFracHelium),1,out);
	fwrite(&(header->dFracNoDomainDecomp),sizeof(header->dFracNoDomainDecomp),1,out);

	return;
}

/***********************************************/
/***********************************************/
/***********************************************/

int main(int argc, char *argv[]) {

	int j;
	CHK_HEADER hh;
	CHK_PART pp;
	char input_file_name[240]="\0", output_file_name[240]="checkpoint.chk", fdl_file_name[240]="\0";
	FILE *InFile, *OutFile, *FdlFile;
	char c1, cc1;
	clock_t t0;

	double dm_softening=-1.0,star_softening=-1.0,gas_softening=-1.0;

	for (j=1; j<argc; j++) {
		if (!strcmp(argv[j],"--help") || !strcmp(argv[j],"-h")) {
			usage();
		}
		else if (!strcmp(argv[j],"-inp")) {
			if (!(++j<argc)) usage();
			strcpy(input_file_name,argv[j]);
		}
		else if (!strcmp(argv[j],"-out")) {
			if (!(++j<argc)) usage();
			strcpy(output_file_name,argv[j]);
		}
		else if (!strcmp(argv[j],"-fdl")) {
			if (!(++j<argc)) usage();
			strcpy(fdl_file_name,argv[j]);
		}
		else if (!strcmp(argv[j],"-dme")) {
			if (!(++j<argc)) usage();
			dm_softening = (double)atof(argv[j]);
		}
		else if (!strcmp(argv[j],"-ge")) {
			if (!(++j<argc)) usage();
			gas_softening = (double)atof(argv[j]);
		}
		else if (!strcmp(argv[j],"-se")) {
			if (!(++j<argc)) usage();
			star_softening = (double)atof(argv[j]);
		}
		else {
			printf("%s is not a valid option. Ignored!\n",argv[j]);
		}
	}

	if(!strcmp(input_file_name,"\0")) {
		fprintf(stderr,"ERROR!\nInput checkpoint name not specified!\n");
		exit(EXIT_FAILURE);
	}
	if(!strcmp(fdl_file_name,"\0")) {
		fprintf(stderr,"ERROR!\n'checkpoint.fdl' file not specified!\n");
		exit(EXIT_FAILURE);
	}

	if ((InFile = fopen(input_file_name,"r")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open '%s' file\n", input_file_name);
		exit(EXIT_FAILURE);
	}
	if ((FdlFile = fopen(fdl_file_name,"r")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open '%s' file\n", fdl_file_name);
		exit(EXIT_FAILURE);
	}
	if ((OutFile = fopen(output_file_name,"w")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open '%s' file\n", output_file_name);
		exit(EXIT_FAILURE);
	}

	fprintf(stdout," @> Copying ASCII header...\n");
	while(!feof(FdlFile)) {
		fread(&c1,sizeof(char),1,FdlFile);
		fread(&cc1,sizeof(char),1,InFile);
		fprintf(OutFile,"%c",c1);
	}
	fseek(OutFile,-2,SEEK_CUR);
	fseek(InFile,-2,SEEK_CUR);
	fclose(FdlFile);

	fprintf(stdout," @> Copying checkpoint header...\n");
	ReadWriteHeader(&hh,InFile,OutFile);

	fprintf(stdout," @> Copying & modifying particle data...\n");
	t0 = clock();
	for (j=0; j<hh.number_of_particles; j++) {
		fread(&pp,sizeof(CHK_PART),1,InFile);
		if ((j < hh.number_of_gas_particles) && (gas_softening > 0.0)) {
			pp.fSoft = gas_softening;
		}
		else if((j < (hh.number_of_gas_particles+hh.number_of_dark_particles)) &&
		(dm_softening > 0.0)) {
			pp.fSoft = dm_softening;
		}
		else if((j < hh.number_of_particles) && (star_softening > 0.0)) {
			pp.fSoft = star_softening;
		}
		fwrite(&pp,sizeof(CHK_PART),1,OutFile);
	}
	fprintf(stdout," @> Particle data copied in %lf s\n\n", (double)(clock() - t0)/CLOCKS_PER_SEC);

	fclose(InFile);
	fclose(OutFile);

	exit(EXIT_SUCCESS);
}

/***********************************************/
/***********************************************/
/***********************************************/
