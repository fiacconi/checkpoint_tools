*** checkpoint_tools : TOOLS TO MANIPULATE GASOLINE CHECKPOINTS ***

AUTHOR: Davide Fiacconi, Institute for Computational Science, University of Zurich

EMAIL: fiacconi AT physik.uzh.ch

VERSION: 1.1.1

This repository contains a bunch of C stand-alone tools to perform operations on the checkpoint
files created by the GASOLINE code (Wadsley et al. 2004; http://arxiv.org/abs/astro-ph/0303521).
Checkpoint files are restart files that contain the information stored in a snapshot, as well as
the data stored in all the auxiliary files. Differently from the snapshot, data are stored in
double precision and therefore checkpoints have larger size than snapshots. Each checkpoint is
made of: (1) an initial header in ASCII format which stores the representation of the structure
of the checkpoint itself, (2) a binary header, and (3) the particle data dumped sequentially per
particle.

More specifically, the repository contains:

- checkpoint.fdl : this is is an auxiliary file that contains the representation of the structure
of a checkpoint. Its content constitutes the initial ASCII header and it is usually used as input
for the programs contained in the repository.

- ChangeSofteningInChkpoint.c : this code is used to modify the gravitational softening of gas,
dark matter, and/or stellar particles within the checkpoint. It produces a new checkpoint with
the new softenings as specified by the user.

- ChangeTimestepInChkpoint.c : this code is used to increase the frequency of time-steps in simulations
that have to be restarted with a checkpoint. It modifies the file IN PLACE (i.e. no output, storing
a copy of the original checkpoint is highly recommended), changing the timestep, the number of
the current output and the total number of output to reach the same final time in the simulation.

- ReconstructChkpoint.c : this code can be used to reconstruct a GASOLINE checkpoint from a given
snapshot with its own auxiliary files (the latter being both in ASCII or in standard binary format).
It produces a new checkpoint that in most conditions (SEE THE HELP OF THE PROGRAM FOR RESTRICTIONS!)
should resemble the checkpoint that the code would have produces at the same timestep.

** HOW TO BUILD: **

The repository contains a Makefile for easy compilation of the codes. The users only requires a
C compiler (e.g. gcc) to build the code. On UNIX system, one just types

...$ make

and the code should compile.

** HOW TO RUN: **

To run, each executable XYZ requires a syntax of the form:

...$ ./XYZ [OPTIONS]

To see the OPTIONS specific to each code, just execute:

...$ ./XYZ -h    OR    ...$ ./XYZ --help

and a brief description of the code and of its options will appear on stdout.

** CHANGES: **

V1.0.0 : initial repository containing ChangeSofteningInChkpoint.c and ChangeTimestepInChkpnt.c

V1.0.1 : renamed file ChangeTimestepInChkpoint.c; added comments in it

V1.0.2 : fixed bugs in Makefile and ChangeTimestepInChkpoint.c

V1.0.3 : Added file "checkpoint.h" to be used by all .c codes

V1.0.4 : Reading of the input parameters improved

V1.0.5 : Modified the name of an attribute of CHK_HEADER to match GASOLINE name

V1.1.0 : Additional program ReconstructChkpoint.c

V1.1.1 : Modified Makefile to compile on Linux. Do not worry if the code produces many warnings,
         it might depend on the option of the compiler.
