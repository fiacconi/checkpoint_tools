################################
# COMPILE THE CHECKPOINT TOOLS #
################################

CC=gcc
CFLAGS=-Wall -O3
LIBS=-lm

SRCS=$(wildcard *.c)
PROGS = $(patsubst %.c,%,$(SRCS))

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LIBS)

clean:
	rm -f $(PROGS)
