#ifndef __CHECKPOINT_H__
#define __CHECKPOINT_H__

#include <rpc/types.h>
#include <rpc/xdr.h>

/****************************************\
 * DEFINITIONS OF STRUCT FOR TIPSY FILE *
\****************************************/

typedef struct {
    float mass;
    float pos[3];
    float vel[3];
    float rho;
    float temp;
    float hsmooth;
    float metals;
    float phi;
} TIPSY_GAS;

typedef struct {
    float mass;
    float pos[3];
    float vel[3];
    float eps;
    float phi;
} TIPSY_DARK;

typedef struct {
    float mass;
    float pos[3];
    float vel[3];
    float metals;
    float tform;
    float eps;
    float phi;
} TIPSY_STAR;

typedef struct {
    double time;
    int nbodies;
    int ndim;
    int nsph;
    int ndark;
    int nstar;
} TIPSY_HEADER;

/*********************************************\
 * DEFINITIONS OF STRUCT FOR CHECKPOINT FILE *
\*********************************************/

typedef struct CoolingParticleStruct {
	double Y_HI,Y_HeI,Y_HeII;
} COOLPARTICLE;

typedef struct chk_particle {
	int iOrder;
	double fMass;
	double fSoft;
	double r[3];
	double v[3];
	double u;
	double fMetals;
	COOLPARTICLE CoolParticle;
	double fTimeCoolIsOffUntil;
	double fTimeForm;
	double fMassForm;
	double fNSN;
	double fMFracOxygen;
	double fMFracIron;
	int iGasOrder;
} CHK_PART;

typedef struct chkHeader {

	int version;
	int not_corrupt_flag;
	int number_of_particles;
	int number_of_gas_particles;
	int number_of_dark_particles;
	int number_of_star_particles;
	int max_order;
	int max_order_gas;
	int max_order_dark;
	int current_timestep;
	double current_time;
	double current_ecosmo;
	double old_time;
	double old_potentiale;
	int opening_type;
	double opening_criterion;
	int number_of_outs;

	int bPeriodic;
	int bComove;
	int bParaRead;
	int bParaWrite;
	int bCannonical;
	int bStandard;
	int bKDK;
	int bBinary;
	int bDoGravity;
	int bDoGas;
	int bFandG;
	int bHeliocentric;
	int bLogHalo;
	int bHernquistSpheroid;
	int bMiyamotoDisk;
	int bRotatingBar;

	double dRotBarMass;
	double dRotBarLength;
	double dRotBarCorotFac;
	double dRotBarTurnOff;
	double dRotBarTurnOn;
	double dRotBarAmpFac;
	double dRotBarDuration;
	double dRotBarPosAng;
	double dRotBarPosX;
	double dRotBarPosY;
	double dRotBarPosZ;
	double dRotBarVelX;
	double dRotBarVelY;
	double dRotBarVelZ;
	double dRotBarTime0;
	double dRotBarOmega;
	double dRotBarB5;
	double dRotBarIz;
	double dRotBarLz;
	double dRotBarLz0;

	int nBucket;
	int iOutInterval;
	int iLogInterval;
	int iCheckInterval;
	int iExpOrder;
	int iEwOrder;
	int nReplicas;
	int iStartStep;
	int nSteps;

	double dExtraStore;
	double dDelta;
	double dEta;
	double dEtaCourant;

	int bEpsAccStep;
	int bNonSymp;
	int iMaxRung;

	double dEwCut;
	double dEwhCut;
	double dPeriod;
	double dxPeriod;
	double dyPeriod;
	double dzPeriod;
	double dHubble0;
	double dOmega0;
	double dLambda;
	double dOmegaRad;
	double dQuintess;
	double dTheta;
	double dTheta2;
	double dCentMass;
	double dMassFracHelium;
	double dFracNoDomainDecomp;
} CHK_HEADER;

#endif
