#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "checkpoint.h"

/***********************************************/
/************ FUNCTION DEFINITIONS *************/
/***********************************************/

void usage() {
	fprintf(stdout,"\n@> USAGE <@\n\n");
	fprintf(stdout,"...$ ./ChangeSofteningInChkpoint -inp <chk_in_filename>\n");
	fprintf(stdout,"                                 -fdl <fdl_filename>\n");
	fprintf(stdout,"                                 -fac <factor_timestep, INTEGER>\n\n");
    fprintf(stdout,"The code takes the GASOLINE checkpoint chk_in_filename and\n");
    fprintf(stdout,"it reads the header. The header is made of a first part in\n");
    fprintf(stdout,"in ASCII, whose structure is contained in fdl_filename\n");
    fprintf(stdout,"(typically the file 'checkpoint.fdl' contained in the\n");
    fprintf(stdout,"'pkdgrav' directory). The code INCREASES the frequency of the\n");
    fprintf(stdout,"timesteps by a factor factor_timestep, as well as the current\n");
    fprintf(stdout,"and the total number of timesteps for consistency.\n\n");
    fprintf(stdout,"!!!     WARNING : THE CHECKPOINT IS MODIFIED IN PLACE    !!!\n");
    fprintf(stdout,"!!! NO ADDITIONAL OUTPUT IS PRODUCED! SAVE A COPY BEFORE !!!\n\n");
	exit(EXIT_FAILURE);
	return;
}

void ReadModifyHeader(CHK_HEADER *header, FILE *in, int factor) {

	/* READING */

	fread(&(header->version),sizeof(header->version),1,in);
	fread(&(header->not_corrupt_flag),sizeof(header->not_corrupt_flag),1,in);
	fread(&(header->number_of_particles),sizeof(header->number_of_particles),1,in);
	fread(&(header->number_of_gas_particles),sizeof(header->number_of_gas_particles),1,in);
	fread(&(header->number_of_dark_particles),sizeof(header->number_of_dark_particles),1,in);
	fread(&(header->number_of_star_particles),sizeof(header->number_of_star_particles),1,in);
	fread(&(header->max_order),sizeof(header->max_order),1,in);
	fread(&(header->max_order_gas),sizeof(header->max_order_gas),1,in);
	fread(&(header->max_order_dark),sizeof(header->max_order_dark),1,in);

	/* READ CURRENT STEP COUNTER */
	fread(&(header->current_timestep),sizeof(header->current_timestep),1,in);
	/* UPDATE CURRENT STEP COUNTER */
	header->current_timestep *= factor;
	/* REPOSITIONING IN FILE */
	fseek(in, -sizeof(header->current_timestep),SEEK_CUR);
	/* OVERWRITE CURRENT STEP COUNTER */
	fwrite(&(header->current_timestep),sizeof(header->current_timestep),1,in);

	fread(&(header->current_time),sizeof(header->current_time),1,in);
	fread(&(header->current_ecosmo),sizeof(header->current_ecosmo),1,in);
	fread(&(header->old_time),sizeof(header->old_time),1,in);
	fread(&(header->old_potentiale),sizeof(header->old_potentiale),1,in);
	fread(&(header->opening_type),sizeof(header->opening_type),1,in);
	fread(&(header->opening_criterion),sizeof(header->opening_criterion),1,in);
	fread(&(header->number_of_outs),sizeof(header->number_of_outs),1,in);

	fread(&(header->bPeriodic),sizeof(header->bPeriodic),1,in);
	fread(&(header->bComove),sizeof(header->bComove),1,in);
	fread(&(header->bParaRead),sizeof(header->bParaRead),1,in);
	fread(&(header->bParaWrite),sizeof(header->bParaWrite),1,in);
	fread(&(header->bCannonical),sizeof(header->bCannonical),1,in);
	fread(&(header->bStandard),sizeof(header->bStandard),1,in);
	fread(&(header->bKDK),sizeof(header->bKDK),1,in);
	fread(&(header->bBinary),sizeof(header->bBinary),1,in);
	fread(&(header->bDoGravity),sizeof(header->bDoGravity),1,in);
	fread(&(header->bDoGas),sizeof(header->bDoGas),1,in);
	fread(&(header->bFandG),sizeof(header->bFandG),1,in);
	fread(&(header->bHeliocentric),sizeof(header->bHeliocentric),1,in);
	fread(&(header->bLogHalo),sizeof(header->bLogHalo),1,in);
	fread(&(header->bHernquistSpheroid),sizeof(header->bHernquistSpheroid),1,in);
	fread(&(header->bMiyamotoDisk),sizeof(header->bMiyamotoDisk),1,in);
	fread(&(header->bRotatingBar),sizeof(header->bRotatingBar),1,in);

	fread(&(header->dRotBarMass),sizeof(header->dRotBarMass),1,in);
	fread(&(header->dRotBarLength),sizeof(header->dRotBarLength),1,in);
	fread(&(header->dRotBarCorotFac),sizeof(header->dRotBarCorotFac),1,in);
	fread(&(header->dRotBarTurnOff),sizeof(header->dRotBarTurnOff),1,in);
	fread(&(header->dRotBarTurnOn),sizeof(header->dRotBarTurnOn),1,in);
	fread(&(header->dRotBarAmpFac),sizeof(header->dRotBarAmpFac),1,in);
	fread(&(header->dRotBarDuration),sizeof(header->dRotBarDuration),1,in);
	fread(&(header->dRotBarPosAng),sizeof(header->dRotBarPosAng),1,in);
	fread(&(header->dRotBarPosX),sizeof(header->dRotBarPosX),1,in);
	fread(&(header->dRotBarPosY),sizeof(header->dRotBarPosY),1,in);
	fread(&(header->dRotBarPosZ),sizeof(header->dRotBarPosZ),1,in);
	fread(&(header->dRotBarVelX),sizeof(header->dRotBarVelX),1,in);
	fread(&(header->dRotBarVelY),sizeof(header->dRotBarVelY),1,in);
	fread(&(header->dRotBarVelZ),sizeof(header->dRotBarVelZ),1,in);
	fread(&(header->dRotBarTime0),sizeof(header->dRotBarTime0),1,in);
	fread(&(header->dRotBarOmega),sizeof(header->dRotBarOmega),1,in);
	fread(&(header->dRotBarB5),sizeof(header->dRotBarB5),1,in);
	fread(&(header->dRotBarIz),sizeof(header->dRotBarIz),1,in);
	fread(&(header->dRotBarLz),sizeof(header->dRotBarLz),1,in);
	fread(&(header->dRotBarLz0),sizeof(header->dRotBarLz0),1,in);

	fread(&(header->nBucket),sizeof(header->nBucket),1,in);
	fread(&(header->iOutInterval),sizeof(header->iOutInterval),1,in);
	fread(&(header->iLogInterval),sizeof(header->iLogInterval),1,in);
	fread(&(header->iCheckInterval),sizeof(header->iCheckInterval),1,in);
	fread(&(header->iExpOrder),sizeof(header->iExpOrder),1,in);
	fread(&(header->iEwOrder),sizeof(header->iEwOrder),1,in);
	fread(&(header->nReplicas),sizeof(header->nReplicas),1,in);
	fread(&(header->iStartStep),sizeof(header->iStartStep),1,in);

	/* READ TOTAL NUMBER OF STEPS */
	fread(&(header->nSteps),sizeof(header->nSteps),1,in);
	/* UPDATE TOTAL NUMBER OF STEPS */
	header->nSteps *= factor;
	/* REPOSITIONING IN FILE */
	fseek(in, -sizeof(header->nSteps),SEEK_CUR);
	/* OVERWRITE TOTAL NUMBER OF STEPS */
	fwrite(&(header->nSteps),sizeof(header->nSteps),1,in);

	fread(&(header->dExtraStore),sizeof(header->dExtraStore),1,in);

	/* READ TIMESTEP */
	fread(&(header->dDelta),sizeof(header->dDelta),1,in);
	/* UPDATE TIMESTEP */
	header->dDelta /= (double)factor;
	/* REPOSITIONING IN FILE */
	fseek(in, -sizeof(header->dDelta),SEEK_CUR);
	/* OVERWRITE TIMESTEP */
	fwrite(&(header->dDelta),sizeof(header->dDelta),1,in);

	fread(&(header->dEta),sizeof(header->dEta),1,in);
	fread(&(header->dEtaCourant),sizeof(header->dEtaCourant),1,in);

	fread(&(header->bEpsAccStep),sizeof(header->bEpsAccStep),1,in);
	fread(&(header->bNonSymp),sizeof(header->bNonSymp),1,in);
	fread(&(header->iMaxRung),sizeof(header->iMaxRung),1,in);

	fread(&(header->dEwCut),sizeof(header->dEwCut),1,in);
	fread(&(header->dEwhCut),sizeof(header->dEwhCut),1,in);
	fread(&(header->dPeriod),sizeof(header->dPeriod),1,in);
	fread(&(header->dxPeriod),sizeof(header->dxPeriod),1,in);
	fread(&(header->dyPeriod),sizeof(header->dyPeriod),1,in);
	fread(&(header->dzPeriod),sizeof(header->dzPeriod),1,in);
	fread(&(header->dHubble0),sizeof(header->dHubble0),1,in);
	fread(&(header->dOmega0),sizeof(header->dOmega0),1,in);
	fread(&(header->dLambda),sizeof(header->dLambda),1,in);
	fread(&(header->dOmegaRad),sizeof(header->dOmegaRad),1,in);
	fread(&(header->dQuintess),sizeof(header->dQuintess),1,in);
	fread(&(header->dTheta),sizeof(header->dTheta),1,in);
	fread(&(header->dTheta2),sizeof(header->dTheta2),1,in);
	fread(&(header->dCentMass),sizeof(header->dCentMass),1,in);
	fread(&(header->dMassFracHelium),sizeof(header->dMassFracHelium),1,in);
	fread(&(header->dFracNoDomainDecomp),sizeof(header->dFracNoDomainDecomp),1,in);

	return;
}

/***********************************************/
/***********************************************/
/***********************************************/

int main(int argc, char *argv[]) {
	int nstep_factor=-1,j;
	CHK_HEADER hh;
	char input_file_name[240]="\0",fdl_file_name[240]="\0";
	FILE *InFile,*FdlFile;
	char c1,cc1;

	for (j=1; j<argc; j++) {
		if (!strcmp(argv[j],"--help") || !strcmp(argv[j],"-h")) {
			usage();
		}
		else if (!strcmp(argv[j],"-inp")) {
			if (!(++j<argc)) usage();
			strcpy(input_file_name,argv[j]);
		}
		else if (!strcmp(argv[j],"-fdl")) {
			if (!(++j<argc)) usage();
			strcpy(fdl_file_name,argv[j]);
		}
		else if (!strcmp(argv[j],"-fac")) {
			if (!(++j<argc)) usage();
			nstep_factor = atoi(argv[j]);
		}
		else {
			printf("%s is not a valid option. Ignored!\n",argv[j]);
		}
	}

	if(!strcmp(input_file_name,"\0")) {
		fprintf(stderr,"ERROR!\nInput checkpoint name not specified!\n");
		exit(EXIT_FAILURE);
	}
	if(!strcmp(fdl_file_name,"\0")) {
		fprintf(stderr,"ERROR!\n'checkpoint.fdl' file not specified!\n");
		exit(EXIT_FAILURE);
	}

	if(nstep_factor < 2) {
		fprintf(stderr,"ERROR!\nFactor to INCREASE nStep should be at least 2!\n");
		exit(EXIT_FAILURE);
	}

	if ((InFile = fopen(input_file_name,"r+")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open '%s' file\n", input_file_name);
		exit(EXIT_FAILURE);
	}
	if ((FdlFile = fopen(fdl_file_name,"r")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open '%s' file\n", fdl_file_name);
		exit(EXIT_FAILURE);
	}

	fprintf(stdout," @> READING ASCII header...\n");
	while(!feof(FdlFile)) {
		fread(&c1,sizeof(char),1,FdlFile);
		fread(&cc1,sizeof(char),1,InFile);
	}
	fseek(InFile,-2,SEEK_CUR);
	fclose(FdlFile);

	fprintf(stdout," @> Reading & modifying checkpoint header...\n\n");
	ReadModifyHeader(&hh,InFile,nstep_factor);

	fclose(InFile);

	exit(EXIT_SUCCESS);
}

/***********************************************/
/***********************************************/
/***********************************************/
