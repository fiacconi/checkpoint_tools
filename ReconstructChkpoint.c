#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
#include "checkpoint.h"

#define EPSCOSMO 1e-7
#define MAXLEV 13
#ifndef FLT_MAX
#define FLT_MAX 3.402823466E+38F
#endif
#define MU_METAL  17.6003
#define CL_Rgascode         8.2494e7
#define CL_Eerg_gm_degK     CL_Rgascode
#define CL_ev_degK          1.0/1.1604e4
#define CL_Eerg_gm_ev       CL_Eerg_gm_degK/CL_ev_degK
#define CL_Eerg_gm_degK3_2  1.5*CL_Eerg_gm_degK

/***********************************************/
/************ FUNCTION DEFINITIONS *************/
/***********************************************/

double temperature2u(COOLPARTICLE *cl, double ZMetal, double temperature) {

    double Y_H, Y_He, Y_tot;

    if (ZMetal <= 0.1) {
        Y_He = (0.236 + 2.1*ZMetal)/4.0;
    }
    else {
        Y_He = (-0.446*(ZMetal - 0.1)/0.9 + 0.446)/4.0;
    }
    Y_H = 1.0 - Y_He*4.0 - ZMetal;
    Y_tot = 2 * Y_H - cl->Y_HI * Y_H + 3 * Y_He - 2 * cl->Y_HeI * Y_He - cl->Y_HeII * Y_He + ZMetal / MU_METAL;

    return (Y_tot*CL_Eerg_gm_degK3_2) * temperature;
}

double dRombergO(double (*func)(double),double a, double b, double eps) {

    double tllnew;
    double tll;
    double tlk[MAXLEV+1];
    int n = 1;
    int nsamples = 1;

    tlk[0] = tllnew = (b-a)*(*func)(0.5*(b+a));
    if(a == b) return tllnew;

    tll = FLT_MAX;

    while((fabs((tllnew-tll)/tllnew) > eps) && (n < MAXLEV)) {
		/*
		 * midpoint rule.
		 */
		double deltax;
		double tlktmp;
		int i;

		nsamples *= 3;
		deltax = (b-a)/nsamples;
		tlktmp = tlk[0];
		tlk[0] = tlk[0]/3.0;

		for(i=0; i<nsamples/3; i++) {
			tlk[0] += deltax*(*func)(a + (3*i + 0.5)*deltax);
			tlk[0] += deltax*(*func)(a + (3*i + 2.5)*deltax);
		}

		/*
		 * Romberg extrapolation.
		 */

		for(i=0; i<n; i++) {
			double tlknew = (pow(9.0, i+1.)*tlk[i] - tlktmp) / (pow(9.0, i+1.) - 1.0);
			if(i+1 < n) tlktmp = tlk[i+1];
			tlk[i+1] = tlknew;
		}
		tll = tllnew;
		tllnew = tlk[n];
		n++;
	}

    if (!(fabs((tllnew-tll)/(tllnew+eps)) < eps)) {
        fprintf(stderr, "ERROR!\nRomberg integration did not converge!\n");
        exit(EXIT_FAILURE);
    }

    return tllnew;
}

double csmExp2Time(double dExp, CHK_HEADER *h) {

    /* This function is almost copied from GASOLINE to try to reconstruct the time in the same way. */

    double dHubble0 = h->dHubble0;
    double dOmega0 = h->dOmega0;
	double dLambda = h->dLambda;
	double dOmegaRad = h->dOmegaRad;
	double dQuintess = h->dQuintess;
    double a0, A, B, eta;

    if(dLambda == 0.0 && dOmegaRad == 0.0 && dQuintess == 0.0) {

        if (dHubble0 < 0.0 || (dHubble0 == 0.0 && !(dOmega0 > 1.0))) {
            fprintf(stderr, "ERROR!\nCosmological parameters have problems!\n");
            exit(EXIT_FAILURE);
        }

        if (dOmega0 == 1.0) {
            if (dExp == 0.0) return 0.0;
            return (2.0/(3.0*dHubble0)*pow(dExp,1.5));
        }
	    else if (dOmega0 > 1.0) {

		    if (dHubble0 == 0.0) {
                B = 1.0/sqrt(dOmega0);
			    eta = acos(1.0-dExp);
			    return (B*(eta-sin(eta)));
            }
		    if (dExp == 0.0) return 0.0;
		    a0 = 1.0/dHubble0/sqrt(dOmega0-1.0);
		    A = 0.5*dOmega0/(dOmega0-1.0);
		    B = A*a0;
		    eta = acos(1.0-dExp/A);
		    return (B*(eta-sin(eta)));
		}
	    else if (dOmega0 > 0.0) {
		    if (dExp == 0.0) return 0.0;
		    a0 = 1.0/dHubble0/sqrt(1.0-dOmega0);
		    A = 0.5*dOmega0/(1.0-dOmega0);
		    B = A*a0;
		    eta = acosh(dExp/A+1.0);
		    return (B*(sinh(eta)-eta));
		}
	    else if (dOmega0 == 0.0) {
		    if (dExp == 0.0) return 0.0;
		    return(dExp/dHubble0);
		}
        else {
            fprintf(stderr, "ERROR!\nCosmological parameters have problems!\n");
            exit(EXIT_FAILURE);
            return 0;
        }
	}
    else {

        double csmCosmoTint(double dY) {
            double dExp = pow(dY, 2.0/3.0);
            double dOmegaCurve = 1.0 - dOmega0 - dLambda - dOmegaRad - dQuintess;
            return 2.0 / (3.0 * dY * (dHubble0 * sqrt(dOmega0 * dExp + dOmegaCurve * dExp * dExp
                                        + dOmegaRad
                                        + dQuintess * dExp * dExp * sqrt(dExp)
                                        + dLambda * dExp * dExp * dExp * dExp) / (dExp*dExp)) );
        }

        return dRombergO((double (*)(double))csmCosmoTint, 0.0, pow(dExp, 1.5), 0.01*EPSCOSMO);
    }
}

void usage() {
	fprintf(stdout,"\n@> USAGE <@\n\n");
	fprintf(stdout,"...$ ./ReconstructChkpoint   -snap <snapshot_in_filename>\n");
	fprintf(stdout,"                             -fdl <fdl_filename>\n");
	fprintf(stdout,"                             -param <parameter_filename>\n");
	fprintf(stdout,"                             -out <chk_out_filename>\n");
    fprintf(stdout,"                             -t0 <initial_time_or_redshift>\n");
	fprintf(stdout,"                             -ascii\n\n");
	fprintf(stdout,"This code takes a GASOLINE STANDARD BINARY snapshot file\n");
	fprintf(stdout,"'snapshot_in_filename' and tries to reconstruct the checkpoint\n");
	fprintf(stdout,"'chk_out_filename' at the time of the snapshot with the aid of\n");
	fprintf(stdout,"the 'fdl_checkpoint' file (typically the file 'checkpoint.fdl'),\n");
	fprintf(stdout,"the parameter file 'parameter_file' of the simulation, and the\n");
	fprintf(stdout,"available auxiliary files (whose format is assumed to be STANDARD\n");
	fprintf(stdout,"BINARY as well; use the '-ascii' flag if the auxiliary files are\n");
	fprintf(stdout,"in ASCII format). The initial time of the run MUST be specified\n");
    fprintf(stdout,"through the parameter -t0. If the simulations is cosmological, -t0\n");
    fprintf(stdout,"passes the initial REDSHIFT of the simulation, otherwise the time\n");
    fprintf(stdout,"in code units.\n");
	fprintf(stdout,"!!!!!!!!!!!!!!!!!!!!!!!!    WARNING    !!!!!!!!!!!!!!!!!!!!!!!!!\n\n");
	fprintf(stdout,"1) Be aware that a perfect reconstruction of the checkpoint in\n");
	fprintf(stdout,"every case is likely impossible! Although this code is tailored\n");
	fprintf(stdout,"to do a good job for cosmological simulations and simulations of\n");
	fprintf(stdout,"isolated galaxies virtually in any condition, there might always be\n");
	fprintf(stdout,"discrepancies between the reconstructed checkpoint and what whould\n");
	fprintf(stdout,"have been the actual checkpoint corresponding to the snapshot.\n");
	fprintf(stdout,"The reason is simple: GASOLINE is a complex code and some of the\n");
	fprintf(stdout,"data might change slightly depending on how the code was compiled\n");
	fprintf(stdout,"or how they are calculated run time, and the reconstructed value\n");
	fprintf(stdout,"might be slightly different from the actual one.\n\n");
	fprintf(stdout,"2) DO NOT USE THIS CODE IF YOU ARE RUNNING A SIMULATION WITH A BAR!\n");
	fprintf(stdout,"GASOLINE, among the many features, gives the possibility to use the\n");
	fprintf(stdout,"the potential of a bar. Some of the parameters of the bar are stored\n");
	fprintf(stdout,"in the header of the checkpoint. However, most of them are not input\n");
	fprintf(stdout,"parameters (i.e. parameters specified in the input parameter file),\n");
	fprintf(stdout,"but they have values calculated by the code during runtime and therfore\n");
	fprintf(stdout,"virually impossible to reconstruct. Those are just initialised to 0!\n\n");
	exit(EXIT_FAILURE);
	return;
}

void read_tipsy_header(char *snapshot_file, int bascii, CHK_HEADER *hh) {
	FILE *f;
	XDR xdr;
	TIPSY_HEADER h;
	char filename[64], *offset;
    int pad = 0, i, max_igasorder;

	fprintf(stdout, " @> Reading TIPSY header from '%s'...\n", snapshot_file);

	/* OPEN FILE AND READ HEADER */
	if ((f=fopen(snapshot_file, "rb")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open file '%s'!\n", snapshot_file);
		exit(EXIT_FAILURE);
	}
	xdrstdio_create(&xdr, f, XDR_DECODE);

    xdr_double(&xdr, &(h.time));
    xdr_int(&xdr, &(h.nbodies));
    xdr_int(&xdr, &(h.ndim));
    xdr_int(&xdr, &(h.nsph));
    xdr_int(&xdr, &(h.ndark));
    xdr_int(&xdr, &(h.nstar));
    xdr_int(&xdr, &pad);

	xdr_destroy(&xdr);
    fclose(f);

	/* FIX CHECKPOINT HEADER PARAMETERS */
	hh->number_of_particles = h.nbodies;
	hh->number_of_gas_particles = h.nsph;
	hh->number_of_dark_particles = h.ndark;
	hh->number_of_star_particles = h.nstar;

	hh->current_time = h.time;
	hh->old_time = h.time;

	/* SET THE TIMESTER NUMBER FROM SNAPSHOT FILE NAME */
	offset = &(snapshot_file[strlen(snapshot_file)-5]);
	strcpy(filename, offset);
	hh->current_timestep = atoi(filename);

	/* USE THE .iord AND .igasord TO DETERMINE THE INDEXES */
	hh->max_order = 0;
	hh->max_order_gas = 0;
	hh->max_order_dark = 0;

	/* .iord first */
	strcpy(filename, snapshot_file);
	strcat(filename, ".iord");

	fprintf(stdout, " @> Reading particle original indexes from '%s'...\n", filename);

	if (bascii) {
		if ((f=fopen(filename, "r")) == NULL) {
			fprintf(stderr,"ERROR!\nCannot open file '%s'!\n", filename);
			exit(EXIT_FAILURE);
		}

		fscanf(f, "%d", &pad);

		for (i=0; i<hh->number_of_gas_particles; ++i) fscanf(f, "%d", &pad);
		hh->max_order_gas = pad;

		for (i=0; i<hh->number_of_dark_particles; ++i) fscanf(f, "%d", &pad);
		hh->max_order_dark = pad;

		for (i=0; i<h.nstar; ++i) fscanf(f, "%d", &pad);
		hh->max_order = pad;

		fclose(f);
	}
	else {
		if ((f=fopen(filename, "rb")) == NULL) {
			fprintf(stderr,"ERROR!\nCannot open file '%s'!\n", filename);
			exit(EXIT_FAILURE);
		}
		xdrstdio_create(&xdr, f, XDR_DECODE);

		fseek(f, sizeof(int) * hh->number_of_gas_particles, SEEK_SET);
		xdr_int(&xdr, &(hh->max_order_gas));

		fseek(f, sizeof(int) * (hh->number_of_gas_particles + hh->number_of_dark_particles), SEEK_SET);
		xdr_int(&xdr, &(hh->max_order_dark));

		fseek(f, sizeof(int) * hh->number_of_particles, SEEK_SET);
		xdr_int(&xdr, &(hh->max_order));

		xdr_destroy(&xdr);
	    fclose(f);
	}

	/* .igasord second, to check max_order_gas */
	strcpy(filename, snapshot_file);
	strcat(filename, ".igasorder");

	fprintf(stdout, " @> Checking max gas original index from '%s'...\n", filename);

	if (bascii) {
		if ((f=fopen(filename, "r")) == NULL) {
			fprintf(stderr,"ERROR!\nCannot open file '%s'!\n", filename);
			exit(EXIT_FAILURE);
		}

		fscanf(f, "%d", &pad);

		for (i=0; i<hh->number_of_gas_particles + hh->number_of_dark_particles; ++i) fscanf(f, "%d", &pad);

		max_igasorder = 0;
		for (i=0; i<h.nstar; ++i) {
			fscanf(f, "%d", &pad);
			max_igasorder = (pad > max_igasorder) ? pad : max_igasorder;
		}

		hh->max_order_gas = (max_igasorder > hh->max_order_gas) ? max_igasorder : hh->max_order_gas;

		fclose(f);
	}
	else {
		if ((f=fopen(filename, "rb")) == NULL) {
			fprintf(stderr,"ERROR!\nCannot open file '%s'!\n", filename);
			exit(EXIT_FAILURE);
		}
		xdrstdio_create(&xdr, f, XDR_DECODE);

		fseek(f, sizeof(int) * (hh->number_of_gas_particles + hh->number_of_dark_particles + 1), SEEK_SET);

		max_igasorder = 0;
		for (i=0; i<h.nstar; ++i) {
			xdr_int(&xdr, &pad);
			max_igasorder = (pad > max_igasorder) ? pad : max_igasorder;
		}

		hh->max_order_gas = (max_igasorder > hh->max_order_gas) ? max_igasorder : hh->max_order_gas;

		xdr_destroy(&xdr);
	    fclose(f);
	}

    return;
}

void read_parameter_file(char *param_file, double t0, CHK_HEADER *h, double *mass_unit, double *length_unit) {
	char buffer[1024], *buf, label[64];
	FILE *f;

	fprintf(stdout, " @> Reading parameter file '%s'...\n", param_file);

	/* OPEN PARAMETER FILE AND CHECK */
	if ((f = fopen(param_file, "r")) == NULL) {
		fprintf(stderr,"ERROR!\nCannot open file '%s'!\n", param_file);
		exit(EXIT_FAILURE);
	}

	/* MAIN LOOP OVER THE PARAMETER FILE */
	while (!feof(f)) {

		/* READ A LINE AND SKIP INITIAL WHITE SPACES, IF ANY */
		fgets(buffer, 1024, f);
		buf = &buffer[0];
		while (isspace(*buf) && !(*buf == '\n')) ++buf;

		/* SKIP COMMENT OR BLANCK LINE */
		if ((buf[0] == '#') || buf[0] == '\n') continue;

		/* READ THE NAME OF THE PARAMETER */
		strncpy(label, buf, strcspn(buf, "=")+1);
		label[strcspn(buf, "=")] = '\0';
		if (strcspn(label, " \t")) label[strcspn(label, " \t")] = '\0';

		if (!strcmp(label, "bPeriodic")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bPeriodic));
		}
		else if (!strcmp(label, "bComove")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bComove));
		}
		else if (!strcmp(label, "bParaRead")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bParaRead));
		}
		else if (!strcmp(label, "bParaWrite")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bParaWrite));
		}
		else if (!strcmp(label, "bCannonical")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bCannonical));
		}
		else if (!strcmp(label, "bStandard")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bStandard));
		}
		else if (!strcmp(label, "bKDK")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bKDK));
		}
		else if (!strcmp(label, "bBinary")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bBinary));
		}
		else if (!strcmp(label, "bDoGravity")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bDoGravity));
		}
		else if (!strcmp(label, "bDoGas")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bDoGas));
		}
		else if (!strcmp(label, "bFandG")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bFandG));
		}
		else if (!strcmp(label, "bHeliocentric")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bHeliocentric));
		}
		else if (!strcmp(label, "bLogHalo")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bLogHalo));
		}
		else if (!strcmp(label, "bHernquistSpheroid")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bHernquistSpheroid));
		}
		else if (!strcmp(label, "bMiyamotoDisk")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bMiyamotoDisk));
		}
		else if (!strcmp(label, "bRotatingBar")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bRotatingBar));
		}
		else if (!strcmp(label, "dRotBarMass")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dRotBarMass));
		}
		else if (!strcmp(label, "dRotBarLength")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dRotBarLength));
		}
		else if (!strcmp(label, "dRotBarCorotFac")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dRotBarCorotFac));
		}
		else if (!strcmp(label, "dRotBarTurnOn")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dRotBarTurnOn));
		}
		else if (!strcmp(label, "dRotBarTurnOff")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dRotBarTurnOff));
		}
		else if (!strcmp(label, "dRotBarDuration")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dRotBarDuration));
		}
		else if (!strcmp(label, "nBucket")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->nBucket));
		}
		else if (!strcmp(label, "iOutInterval")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iOutInterval));
		}
		else if (!strcmp(label, "iLogInterval")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iLogInterval));
		}
		else if (!strcmp(label, "iCheckInterval")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iCheckInterval));
		}
		else if (!strcmp(label, "iOrder")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iExpOrder));
		}
		else if (!strcmp(label, "iEwOrder")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iEwOrder));
		}
		else if (!strcmp(label, "nReplicas")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->nReplicas));
		}
		else if (!strcmp(label, "iStartStep")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iStartStep));
		}
		else if (!strcmp(label, "nSteps")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->nSteps));
		}
		else if (!strcmp(label, "dExtraStore")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dExtraStore));
		}
		else if (!strcmp(label, "dDelta")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dDelta));
		}
		else if (!strcmp(label, "dEta")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dEta));
		}
		else if (!strcmp(label, "dEtaCourant")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dEtaCourant));
		}
		else if (!strcmp(label, "bEpsAccStep")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bEpsAccStep));
		}
		else if (!strcmp(label, "bNonSymp")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->bNonSymp));
		}
		else if (!strcmp(label, "iMaxRung")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%d", &(h->iMaxRung));
		}
		else if (!strcmp(label, "dEwCut")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dEwCut));
		}
		else if (!strcmp(label, "dEwhCut")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dEwhCut));
		}
		else if (!strcmp(label, "dPeriod")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dPeriod));
		}
		else if (!strcmp(label, "dxPeriod")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dxPeriod));
		}
		else if (!strcmp(label, "dyPeriod")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dyPeriod));
		}
		else if (!strcmp(label, "dzPeriod")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dzPeriod));
		}
		else if (!strcmp(label, "dHubble0")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dHubble0));
		}
		else if (!strcmp(label, "dOmega0")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dOmega0));
		}
		else if (!strcmp(label, "dLambda")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dLambda));
		}
		else if (!strcmp(label, "dOmegaRad")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dOmegaRad));
		}
		else if (!strcmp(label, "dQuintess")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dQuintess));
		}
		else if (!strcmp(label, "dTheta")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dTheta));
		}
		else if (!strcmp(label, "dTheta2")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dTheta2));
		}
		else if (!strcmp(label, "dCentMass")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dCentMass));
		}
		else if (!strcmp(label, "dMassFracHelium")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dMassFracHelium));
		}
		else if (!strcmp(label, "dFracNoDomainDecomp")) {
			sscanf(&buf[strcspn(buf, "=")+1], "%lf", &(h->dFracNoDomainDecomp));
		}
        else if (!strcmp(label, "dMsolUnit")) {
            sscanf(&buf[strcspn(buf, "=")+1], "%lf", mass_unit);
        }
        else if (!strcmp(label, "dKpcUnit")) {
            sscanf(&buf[strcspn(buf, "=")+1], "%lf", length_unit);
        }
	}

	fclose(f);

	/* ADDITIONAL FIXES TO THE PARAMTERS */
	/* I assume that a cosmological simulation is identified by bComove=TRUE! */

	if ((h->dTheta > 0) || (h->dTheta2 > 0)) {
		h->opening_type = 1;
		h->opening_criterion = h->dTheta;
		if (h->bComove && h->current_time >= 1./3.) {
			h->opening_criterion = h->dTheta2;
		}
	}

	if (h->bComove) {

        fprintf(stdout, " @> Adopted cosmological parameters:\n");
        fprintf(stdout, " - dHubble0 = %lf\n", h->dHubble0);
        fprintf(stdout, " - dOmega0 = %lf\n", h->dOmega0);
        fprintf(stdout, " - dLambda = %lf\n", h->dLambda);
        fprintf(stdout, " - dOmegaRad = %lf\n", h->dOmegaRad);
        fprintf(stdout, " - dQuintess = %lf\n", h->dQuintess);

        h->current_time = csmExp2Time(h->current_time, h);
    	h->old_time = h->current_time;
	}

    h->dRotBarTime0 = h->current_time;

	if (h->dDelta == 0.0) {
		if (h->bComove) {
            double initial_time = csmExp2Time(1./(1. + t0), h);
            h->dDelta = (h->current_time - initial_time) / (h->current_timestep - h->iStartStep);
		}
		else {
			h->dDelta = (h->current_time - t0) / (h->current_timestep - h->iStartStep);
		}
	}

    if ((*mass_unit) * (*length_unit) == 0.0) {
        fprintf(stderr, "ERROR!\nCould not read mass and length units from parameter file '%s'!\n", param_file);
        exit(EXIT_FAILURE);
    }
    *mass_unit *= 1.9891e33;
    *length_unit *= 3.08567e21;

	return;
}

void init_checkpoint_header(char *param_file, char *snap_file, int bascii, double t0, CHK_HEADER *h, double *mass_unit, double *length_unit) {

	fprintf(stdout, " @> Initializing checkpoint header...\n");

	/* INITIALISE ALL VALUES ACCORDING TO master.c IN GASOLINE, WHEN POSSIBLE */
	h->version = 8; /* Latest checkpoint version for GASOLINE */
	h->not_corrupt_flag = 1;

	/* All this parameters can be fixed later, after reading the snapshot header and the parameter file */
	h->number_of_particles = 0;
	h->number_of_gas_particles = 0;
	h->number_of_dark_particles = 0;
	h->number_of_star_particles = 0;
	h->max_order = 0;
	h->max_order_gas = 0;
	h->max_order_dark = 0;
	h->current_timestep = 0;
	h->current_time = 0.0;
	h->current_ecosmo = 0.0;
	h->old_time = 0.0;
	h->old_potentiale = 0.0;
	h->opening_type = 0;
	h->opening_criterion = 0;
	h->number_of_outs = 0;
	/* ---- */

	h->bPeriodic = 0;
	h->bComove = 0;
	h->bParaRead = 1;
	h->bParaWrite = 1;
	h->bCannonical = 1;
	h->bStandard = 0;
	h->bKDK = 1;
	h->bBinary = 1;
	h->bDoGravity = 1;
	h->bDoGas = 1;
	h->bFandG = 0;
	h->bHeliocentric = 0;
	h->bLogHalo = 0;
	h->bHernquistSpheroid = 0;
	h->bMiyamotoDisk = 0;
	h->bRotatingBar = 0;

	/* PROBLEM WITH THE BAR PARAMETERS: most of the parameters dumped for the bar
	 * are not input parameters, but values calculated by the code during the run.
	 * Since I cannot reconstruct them, I put them to 0. This means: DO NOT USE
	 * THIS CODE TO RECONSTRUCT THE CHECKPOINT OF A SIMULATION THAT USED A BAR!!!
	 */
	h->dRotBarMass = 0.0;
	h->dRotBarLength = 0.0;
	h->dRotBarCorotFac = 0.0;
	h->dRotBarTurnOff = 0.0;
	h->dRotBarTurnOn = 0.0;
	h->dRotBarAmpFac = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarDuration = 0.0;
	h->dRotBarPosAng = 0.0;
	h->dRotBarPosX = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarPosY = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarPosZ = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarVelX = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarVelY = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarVelZ = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarTime0 = 0.0;
	h->dRotBarOmega = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarB5 = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarIz = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarLz = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */
	h->dRotBarLz0 = 0.0; /* This parameter is calculated by the code, not input parameter! I assume 0.0 */

	h->nBucket = 8;
	h->iOutInterval = 0;
	h->iLogInterval = 10;
	h->iCheckInterval = 10;
	h->iExpOrder = 4;
	h->iEwOrder = 4;
	h->nReplicas = 0;
	h->iStartStep = 0;
	h->nSteps = 0;

	h->dExtraStore = 0.1;
	h->dDelta = 0.0;
	h->dEta = 0.1;
	h->dEtaCourant = 0.4;

	h->bEpsAccStep = 1;
	h->bNonSymp = 1;
	h->iMaxRung = 1;

	h->dEwCut = 2.6;
	h->dEwhCut = 2.8;
	h->dPeriod = 1.0;
	h->dxPeriod = 1.0;
	h->dyPeriod = 1.0;
	h->dzPeriod = 1.0;
	h->dHubble0 = 0.0;
	h->dOmega0 = 1.0;
	h->dLambda = 0.0;
	h->dOmegaRad = 0.0;
	h->dQuintess = 0.0;
	h->dTheta = 0.8;
	h->dTheta2 = 0.8;
	h->dCentMass = 1.0;
	h->dMassFracHelium = 0.236; /* For COOLING_METAL! This is initialised to 0.25 if COOLING_COSMO is used! */
	h->dFracNoDomainDecomp = 0.002;

	/* INITIALIZE THE CHECKPOINT PARAMETERS FROM THE SNAPSHOT HEADER */
	read_tipsy_header(snap_file, bascii, h);

	/* INITIALIZE THE CHECKPOINT PARAMETERS FROM THE PARAMETER FILE */
	read_parameter_file(param_file, t0, h, mass_unit, length_unit);

	fprintf(stdout, " @> Checkpoint header intialised succesfully!\n");

	return;
}

void write_checkpoint(char *fdl_file, char *snap_file, char *out_file, int bascii, double mu, double lu, CHK_HEADER *h) {

    FILE *fchk, *fdl, *tipsy;
    FILE *iord, *igasord, *oxfrac, *fefrac, *hi, *hei, *heii, *massform, *timeform, *coolontime;
    XDR xdr, iord_xdr, igasord_xdr, oxfrac_xdr, fefrac_xdr, hi_xdr, hei_xdr, heii_xdr, massform_xdr, timeform_xdr, coolontime_xdr;
    CHK_PART cp;
    TIPSY_GAS gp;
    TIPSY_DARK dp;
    TIPSY_STAR sp;
    char c1, auxname[240], readmode[8];
    int i, j, ival;
    float rval;

    double spec_energy_unit = 6.67408e-8 * mu / lu;

    fprintf(stdout," @> WRITING THE RECONSTRUCTED CHECKPOINT...\n");

    /* OPEN AND CHECK THE CHECKPOINT */
    if ((fchk = fopen(out_file, "wb")) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", out_file);
        exit(EXIT_FAILURE);
    }

    /* FIRST, COPY THE ASCII HEADER FROM checkpoint.fdl */
    fprintf(stdout,"    --- Copying ASCII header...\n");
    if ((fdl = fopen(fdl_file, "r")) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", fdl_file);
        exit(EXIT_FAILURE);
    }
	while(!feof(fdl)) {
		fread(&c1, sizeof(char), 1, fdl);
		fprintf(fchk, "%c", c1);
	}
	fseek(fchk, -2, SEEK_CUR);
	fclose(fdl); /* CLOSE THE .fdl FILE */

    /* SECOND, DUMPS THE CHECKPOINT HEADER */
    fprintf(stdout,"    --- Dumping the checkpoint header...\n");

    fwrite(&(h->version), sizeof(h->version), 1, fchk);
	fwrite(&(h->not_corrupt_flag), sizeof(h->not_corrupt_flag), 1, fchk);
	fwrite(&(h->number_of_particles), sizeof(h->number_of_particles), 1, fchk);
	fwrite(&(h->number_of_gas_particles), sizeof(h->number_of_gas_particles), 1, fchk);
	fwrite(&(h->number_of_dark_particles), sizeof(h->number_of_dark_particles), 1, fchk);
	fwrite(&(h->number_of_star_particles), sizeof(h->number_of_star_particles), 1, fchk);
	fwrite(&(h->max_order), sizeof(h->max_order), 1, fchk);
	fwrite(&(h->max_order_gas), sizeof(h->max_order_gas), 1, fchk);
	fwrite(&(h->max_order_dark), sizeof(h->max_order_dark), 1, fchk);
	fwrite(&(h->current_timestep), sizeof(h->current_timestep), 1, fchk);
	fwrite(&(h->current_time), sizeof(h->current_time), 1, fchk);
	fwrite(&(h->current_ecosmo), sizeof(h->current_ecosmo), 1, fchk);
	fwrite(&(h->old_time), sizeof(h->old_time), 1, fchk);
	fwrite(&(h->old_potentiale), sizeof(h->old_potentiale), 1, fchk);
	fwrite(&(h->opening_type), sizeof(h->opening_type), 1, fchk);
	fwrite(&(h->opening_criterion), sizeof(h->opening_criterion), 1, fchk);
	fwrite(&(h->number_of_outs), sizeof(h->number_of_outs), 1, fchk);
	fwrite(&(h->bPeriodic), sizeof(h->bPeriodic), 1, fchk);
	fwrite(&(h->bComove), sizeof(h->bComove), 1, fchk);
	fwrite(&(h->bParaRead), sizeof(h->bParaRead), 1, fchk);
	fwrite(&(h->bParaWrite), sizeof(h->bParaWrite), 1, fchk);
	fwrite(&(h->bCannonical), sizeof(h->bCannonical), 1, fchk);
	fwrite(&(h->bStandard), sizeof(h->bStandard), 1, fchk);
	fwrite(&(h->bKDK), sizeof(h->bKDK), 1, fchk);
	fwrite(&(h->bBinary), sizeof(h->bBinary), 1, fchk);
	fwrite(&(h->bDoGravity), sizeof(h->bDoGravity), 1, fchk);
	fwrite(&(h->bDoGas), sizeof(h->bDoGas), 1, fchk);
	fwrite(&(h->bFandG), sizeof(h->bFandG), 1, fchk);
	fwrite(&(h->bHeliocentric), sizeof(h->bHeliocentric), 1, fchk);
	fwrite(&(h->bLogHalo), sizeof(h->bLogHalo), 1, fchk);
	fwrite(&(h->bHernquistSpheroid), sizeof(h->bHernquistSpheroid), 1, fchk);
	fwrite(&(h->bMiyamotoDisk), sizeof(h->bMiyamotoDisk), 1, fchk);
	fwrite(&(h->bRotatingBar), sizeof(h->bRotatingBar), 1, fchk);
	fwrite(&(h->dRotBarMass), sizeof(h->dRotBarMass), 1, fchk);
	fwrite(&(h->dRotBarLength), sizeof(h->dRotBarLength), 1, fchk);
	fwrite(&(h->dRotBarCorotFac), sizeof(h->dRotBarCorotFac), 1, fchk);
	fwrite(&(h->dRotBarTurnOff), sizeof(h->dRotBarTurnOff), 1, fchk);
	fwrite(&(h->dRotBarTurnOn), sizeof(h->dRotBarTurnOn), 1, fchk);
	fwrite(&(h->dRotBarAmpFac), sizeof(h->dRotBarAmpFac), 1, fchk);
	fwrite(&(h->dRotBarDuration), sizeof(h->dRotBarDuration), 1, fchk);
	fwrite(&(h->dRotBarPosAng), sizeof(h->dRotBarPosAng), 1, fchk);
	fwrite(&(h->dRotBarPosX), sizeof(h->dRotBarPosX), 1, fchk);
	fwrite(&(h->dRotBarPosY), sizeof(h->dRotBarPosY), 1, fchk);
	fwrite(&(h->dRotBarPosZ), sizeof(h->dRotBarPosZ), 1, fchk);
	fwrite(&(h->dRotBarVelX), sizeof(h->dRotBarVelX), 1, fchk);
	fwrite(&(h->dRotBarVelY), sizeof(h->dRotBarVelY), 1, fchk);
	fwrite(&(h->dRotBarVelZ), sizeof(h->dRotBarVelZ), 1, fchk);
	fwrite(&(h->dRotBarTime0), sizeof(h->dRotBarTime0), 1, fchk);
	fwrite(&(h->dRotBarOmega), sizeof(h->dRotBarOmega), 1, fchk);
	fwrite(&(h->dRotBarB5), sizeof(h->dRotBarB5), 1, fchk);
	fwrite(&(h->dRotBarIz), sizeof(h->dRotBarIz), 1, fchk);
	fwrite(&(h->dRotBarLz), sizeof(h->dRotBarLz), 1, fchk);
	fwrite(&(h->dRotBarLz0), sizeof(h->dRotBarLz0), 1, fchk);
	fwrite(&(h->nBucket), sizeof(h->nBucket), 1, fchk);
	fwrite(&(h->iOutInterval), sizeof(h->iOutInterval), 1, fchk);
	fwrite(&(h->iLogInterval), sizeof(h->iLogInterval), 1, fchk);
	fwrite(&(h->iCheckInterval), sizeof(h->iCheckInterval), 1, fchk);
	fwrite(&(h->iExpOrder), sizeof(h->iExpOrder), 1, fchk);
	fwrite(&(h->iEwOrder), sizeof(h->iEwOrder), 1, fchk);
	fwrite(&(h->nReplicas), sizeof(h->nReplicas), 1, fchk);
	fwrite(&(h->iStartStep), sizeof(h->iStartStep), 1, fchk);
	fwrite(&(h->nSteps), sizeof(h->nSteps), 1, fchk);
	fwrite(&(h->dExtraStore), sizeof(h->dExtraStore), 1, fchk);
	fwrite(&(h->dDelta), sizeof(h->dDelta), 1, fchk);
	fwrite(&(h->dEta), sizeof(h->dEta), 1, fchk);
	fwrite(&(h->dEtaCourant), sizeof(h->dEtaCourant), 1, fchk);
	fwrite(&(h->bEpsAccStep), sizeof(h->bEpsAccStep), 1, fchk);
	fwrite(&(h->bNonSymp), sizeof(h->bNonSymp), 1, fchk);
	fwrite(&(h->iMaxRung), sizeof(h->iMaxRung), 1, fchk);
	fwrite(&(h->dEwCut), sizeof(h->dEwCut), 1, fchk);
	fwrite(&(h->dEwhCut), sizeof(h->dEwhCut), 1, fchk);
	fwrite(&(h->dPeriod), sizeof(h->dPeriod), 1, fchk);
	fwrite(&(h->dxPeriod), sizeof(h->dxPeriod), 1, fchk);
	fwrite(&(h->dyPeriod), sizeof(h->dyPeriod), 1, fchk);
	fwrite(&(h->dzPeriod), sizeof(h->dzPeriod), 1, fchk);
	fwrite(&(h->dHubble0), sizeof(h->dHubble0), 1, fchk);
	fwrite(&(h->dOmega0), sizeof(h->dOmega0), 1, fchk);
	fwrite(&(h->dLambda), sizeof(h->dLambda), 1, fchk);
	fwrite(&(h->dOmegaRad), sizeof(h->dOmegaRad), 1, fchk);
	fwrite(&(h->dQuintess), sizeof(h->dQuintess), 1, fchk);
	fwrite(&(h->dTheta), sizeof(h->dTheta), 1, fchk);
	fwrite(&(h->dTheta2), sizeof(h->dTheta2), 1, fchk);
	fwrite(&(h->dCentMass), sizeof(h->dCentMass), 1, fchk);
	fwrite(&(h->dMassFracHelium), sizeof(h->dMassFracHelium), 1, fchk);
	fwrite(&(h->dFracNoDomainDecomp), sizeof(h->dFracNoDomainDecomp), 1, fchk);

    /* THIRD, OPEN THE AUXILIARY FILES ADN SKIP NUMBER OF PARTICLES */
    fprintf(stdout,"    --- Opening the auxiliary files...\n");

    if (bascii) strcpy(readmode, "r");
    else strcpy(readmode, "rb");

    strcpy(auxname, snap_file);
    strcat(auxname, ".iord");
    if ((iord = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(iord, "%d", &ival);
    else {
        xdrstdio_create(&iord_xdr, iord, XDR_DECODE);
        xdr_int(&iord_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".igasorder");
    if ((igasord = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(igasord, "%d", &ival);
    else {
        xdrstdio_create(&igasord_xdr, igasord, XDR_DECODE);
        xdr_int(&igasord_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".OxMassFrac");
    if ((oxfrac = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(oxfrac, "%d", &ival);
    else {
        xdrstdio_create(&oxfrac_xdr, oxfrac, XDR_DECODE);
        xdr_int(&oxfrac_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".FeMassFrac");
    if ((fefrac = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(fefrac, "%d", &ival);
    else {
        xdrstdio_create(&fefrac_xdr, fefrac, XDR_DECODE);
        xdr_int(&fefrac_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".HI");
    if ((hi = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(hi, "%d", &ival);
    else {
        xdrstdio_create(&hi_xdr, hi, XDR_DECODE);
        xdr_int(&hi_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".HeI");
    if ((hei = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(hei, "%d", &ival);
    else {
        xdrstdio_create(&hei_xdr, hei, XDR_DECODE);
        xdr_int(&hei_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".HeII");
    if ((heii = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(heii, "%d", &ival);
    else {
        xdrstdio_create(&heii_xdr, heii, XDR_DECODE);
        xdr_int(&heii_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".massform");
    if ((massform = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(massform, "%d", &ival);
    else {
        xdrstdio_create(&massform_xdr, massform, XDR_DECODE);
        xdr_int(&massform_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".timeform");
    if ((timeform = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(timeform, "%d", &ival);
    else {
        xdrstdio_create(&timeform_xdr, timeform, XDR_DECODE);
        xdr_int(&timeform_xdr, &ival);
    }

    strcpy(auxname, snap_file);
    strcat(auxname, ".coolontime");
    if ((coolontime = fopen(auxname, readmode)) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", auxname);
        exit(EXIT_FAILURE);
    }
    if (bascii) fscanf(coolontime, "%d", &ival);
    else {
        xdrstdio_create(&coolontime_xdr, coolontime, XDR_DECODE);
        xdr_int(&coolontime_xdr, &ival);
    }

    /* OPENING SNAPSHOT FILE AND SKIPPING THE HEADER */
    fprintf(stdout,"    --- Opening the snapshot file...\n");

    if ((tipsy = fopen(snap_file, "rb")) == NULL) {
        fprintf(stderr, "ERROR!\nCannot open file '%s'!\n", snap_file);
        exit(EXIT_FAILURE);
    }
    xdrstdio_create(&xdr, tipsy, XDR_DECODE);
    fseek(tipsy, 32, SEEK_SET);

    /* MAIN READING/WRITING LOOP */
    fprintf(stdout,"    --- Main loop over the particles...\n");

    cp.fNSN = 0.0;
    for (i=0, ival=1; i<h->number_of_particles; ++i) {

        /* READ DATA FROM AUXILIARY FILES */
        if (bascii) fscanf(iord, "%d", &(cp.iOrder));
        else xdr_int(&iord_xdr, &(cp.iOrder));

        if (bascii) fscanf(igasord, "%d", &(cp.iGasOrder));
        else xdr_int(&igasord_xdr, &(cp.iGasOrder));

        if (bascii) fscanf(oxfrac, "%lf", &(cp.fMFracOxygen));
        else {
            xdr_float(&oxfrac_xdr, &rval);
            cp.fMFracOxygen = (double)rval;
        }

        if (bascii) fscanf(fefrac, "%lf", &(cp.fMFracIron));
        else {
            xdr_float(&fefrac_xdr, &rval);
            cp.fMFracIron = (double)rval;
        }

        if (bascii) fscanf(hi, "%lf", &(cp.CoolParticle.Y_HI));
        else {
            xdr_float(&hi_xdr, &rval);
            cp.CoolParticle.Y_HI = (double)rval;
        }

        if (bascii) fscanf(hei, "%lf", &(cp.CoolParticle.Y_HeI));
        else {
            xdr_float(&hei_xdr, &rval);
            cp.CoolParticle.Y_HeI = (double)rval;
        }

        if (bascii) fscanf(heii, "%lf", &(cp.CoolParticle.Y_HeII));
        else {
            xdr_float(&heii_xdr, &rval);
            cp.CoolParticle.Y_HeII = (double)rval;
        }

        if (bascii) fscanf(massform, "%lf", &(cp.fMassForm));
        else {
            xdr_float(&massform_xdr, &rval);
            cp.fMassForm = (double)rval;
        }

        if (bascii) fscanf(timeform, "%lf", &(cp.fTimeForm));
        else {
            xdr_float(&timeform_xdr, &rval);
            cp.fTimeForm = (double)rval;
        }

        if (bascii) fscanf(coolontime, "%lf", &(cp.fTimeCoolIsOffUntil));
        else {
            xdr_float(&coolontime_xdr, &rval);
            cp.fTimeCoolIsOffUntil = (double)rval;
        }

        /* READ PARTICLE FROM SNAPSHOT FILE */
        if (i < h->number_of_gas_particles) {
            xdr_vector(&xdr, (char *)(&gp), (sizeof(TIPSY_GAS)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
            cp.fMass = (double)gp.mass;
            cp.fSoft = (double)gp.hsmooth;
            for (j=0; j<3; ++j) {
                cp.r[j] = (double)gp.pos[j];
                cp.v[j] = (double)gp.vel[j];
            }
        	cp.u = temperature2u(&(cp.CoolParticle), cp.fMetals, (double)gp.temp) / spec_energy_unit;
        	cp.fMetals = (double)gp.metals;
        }
        else if (i < h->number_of_gas_particles + h->number_of_dark_particles) {
            xdr_vector(&xdr, (char *)(&dp), (sizeof(TIPSY_DARK)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
            cp.fMass = (double)dp.mass;
            cp.fSoft = (double)dp.eps;
            for (j=0; j<3; ++j) {
                cp.r[j] = (double)dp.pos[j];
                cp.v[j] = (double)dp.vel[j];
            }
            cp.u = 0.0;
        	cp.fMetals = 0.0;
        }
        else {
            xdr_vector(&xdr, (char *)(&sp), (sizeof(TIPSY_STAR)/sizeof(float)), sizeof(float), (xdrproc_t)xdr_float);
            cp.fMass = (double)sp.mass;
            cp.fSoft = (double)sp.eps;
            for (j=0; j<3; ++j) {
                cp.r[j] = (double)sp.pos[j];
                cp.v[j] = (double)sp.vel[j];
            }
            cp.u = 0.0;
        	cp.fMetals = (double)sp.metals;
        }

        /* WRITE THE CHECKPOINT PARTICLE */
        fwrite(&cp, sizeof(cp), 1, fchk);

        if (i == (h->number_of_particles/20) * ival) {
            fprintf(stdout, ".");
            fflush(stdout);
            ++ival;
        }
    }

    /* CLOSE THE CHECKPOINT, THE SNAPSHOT, AND THE AUXILIARY FILES*/
    if (!bascii) {
        xdr_destroy(&iord_xdr);
        xdr_destroy(&igasord_xdr);
        xdr_destroy(&oxfrac_xdr);
        xdr_destroy(&fefrac_xdr);
        xdr_destroy(&hi_xdr);
        xdr_destroy(&hei_xdr);
        xdr_destroy(&heii_xdr);
        xdr_destroy(&massform_xdr);
        xdr_destroy(&timeform_xdr);
        xdr_destroy(&coolontime_xdr);
    }
    xdr_destroy(&xdr);

    fclose(fchk);
    fclose(tipsy);
    fclose(iord);
    fclose(igasord);
    fclose(oxfrac);
    fclose(fefrac);
    fclose(hi);
    fclose(hei);
    fclose(heii);
    fclose(massform);
    fclose(timeform);
    fclose(coolontime);

    fprintf(stdout, "\n @> RECONSTRUCTED CHECKPOINT WRITTEN CORRECTLY!\n");

    return;
}

void read_input_parameters(int argc, char **argv,
							char *snap_file_name,
							char *fdl_file_name,
							char *param_file_name,
							char *out_file_name,
                            double *t0,
							int *bascii) {
	int j;

	*bascii = 0;

	for (j=1; j<argc; j++) {
		if (!strcmp(argv[j],"--help") || !strcmp(argv[j],"-h")) {
			usage();
		}
		else if (!strcmp(argv[j],"-snap")) {
			if (!(++j<argc)) usage();
			strcpy(snap_file_name, argv[j]);
		}
		else if (!strcmp(argv[j],"-fdl")) {
			if (!(++j<argc)) usage();
			strcpy(fdl_file_name, argv[j]);
		}
		else if (!strcmp(argv[j],"-param")) {
			if (!(++j<argc)) usage();
			strcpy(param_file_name, argv[j]);
		}
		else if (!strcmp(argv[j],"-out")) {
			if (!(++j<argc)) usage();
			strcpy(out_file_name, argv[j]);
		}
        else if (!strcmp(argv[j],"-t0")) {
            if (!(++j<argc)) usage();
			*t0 = (double)atof(argv[j]);
		}
		else if (!strcmp(argv[j],"-ascii")) {
			*bascii = 1;
		}
		else {
			printf("%s is not a valid option. Ignored!\n",argv[j]);
		}
	}

	return;
}

/***********************************************/
/***********************************************/
/***********************************************/

int main(int argc, char *argv[]) {

	int ascii_aux;
    double t0 = -99.0, mass_unit = 0.0, length_unit = 0.0;
	char snapshot_file_name[240]="\0", output_file_name[240]="checkpoint.chk", fdl_file_name[240]="\0", param_file_name[240]="\0";
	CHK_HEADER header_chk;
    clock_t t1;

	/* READ INPUT PARAMETERS AND CHECK */
	read_input_parameters(argc, argv, snapshot_file_name, fdl_file_name, param_file_name, output_file_name, &t0, &ascii_aux);

	if(!strcmp(snapshot_file_name,"\0")) {
		fprintf(stderr,"ERROR!\nInput snapshot file name not specified!\n");
		usage();
	}
	if(!strcmp(fdl_file_name,"\0")) {
		fprintf(stderr,"ERROR!\n'checkpoint.fdl' file not specified!\n");
		usage();
	}
	if(!strcmp(param_file_name,"\0")) {
		fprintf(stderr,"ERROR!\nGasoline parameter file not specified!\n");
		usage();
	}
	if(!strcmp(output_file_name,"\0")) {
		fprintf(stderr,"ERROR!\nOutput checkpoint name not specified!\n");
		usage();
	}
    if(t0 == -99.) {
		fprintf(stderr,"ERROR!\nInitial time or redshift not specified!\n");
		usage();
	}

	/* INIT THE CHECKPOINT HEADER */
	init_checkpoint_header(param_file_name, snapshot_file_name, ascii_aux, t0, &header_chk, &mass_unit, &length_unit);

    /* READ DATA AND WRITE THE CHECKPOINT */
    t1 = clock();
    write_checkpoint(fdl_file_name, snapshot_file_name, output_file_name, ascii_aux, mass_unit, length_unit, &header_chk);
    fprintf(stdout, " @> RECONSTRUCTED CHECKPOINT WRITTEN IN %g s!\n", (double)(clock() - t1)/CLOCKS_PER_SEC);

	exit(EXIT_SUCCESS);
}

/***********************************************/
/***********************************************/
/***********************************************/
